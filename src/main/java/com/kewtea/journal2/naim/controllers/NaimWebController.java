package com.kewtea.journal2.naim.controllers;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;



@Controller
@RequestMapping("/naim")  //naim.ai, ad.naim.ai
public class NaimWebController {
	
	Logger logger = LoggerFactory.getLogger(NaimWebController.class);
	
	
	//원하는 데이터들을 application.properties에서 받아서 주입
	@Value("#{${naim.valuesMap}}")
	private Map<String, String> valuesMap;
	
	@ModelAttribute("valuesMap2")
	public Map<String, String> InitData(){
		Map<String, String> valuesMap2 = new HashMap<String, String>();
		logger.info("naim.valuesMpa.key1:"+valuesMap.get("key1"));
		valuesMap2.put("key1", valuesMap.get("key1"));
		valuesMap2.put("name", "james bond");
		return valuesMap2;
	}
	
	//현재 naim은 String 대신에 mav를 전달 함. values를 받고, private ModelAndView buildModelAndView(ModelAndView mav) { ...mav.setViewName("about");..return mav
	
	@RequestMapping(value="/test/{page_name}")
	public String getnaimTestPages(@PathVariable("page_name") String page_name,@ModelAttribute("valuesMap2") Map<String, String> valuesMap2, Model model, HttpServletRequest request) {
		
		logger.info("page_name : "+page_name);
		String request_path = request.getServletPath();
		logger.info("the url path requested as page is:"+request_path);
		model.addAttribute("type","index");
		String pageString="naim/index_old";
		switch(pageString){
		case "index":
			pageString="naim/index_old";
			break;
		case "about":
			model.addAttribute("type","about");
			pageString="naim/index_old";  //"naim/about_old";
			break;
		case "ad":
			model.addAttribute("type","ad");
			pageString="naim/index_old";  //"naim/ad_old";
			break;
		default:
			break;
		}
		
		return pageString;
	}
	
	@RequestMapping(value="/testp/{product_id}")
	public String getnaimProductTestPages(@PathVariable("product_id") String product_id, Model model, HttpServletRequest request) {
		logger.info("product_id from path : "+product_id);
		String request_path = request.getServletPath();
		logger.info("the product url path requested as product_id is:"+request_path);
		model.addAttribute("product_id","1");
		String pageString="naim/product_old";
		
		return pageString;
	}
	
	//잘못된 url 처리 
	@RequestMapping(value = "**")
	public String redirectToDefault(Model model, HttpServletRequest request) {
		String request_path = request.getServletPath();
		logger.info("caught up by this : "+request_path);
		model.addAttribute("type","index");
		//return "redirect:/naim/test/index"; or forward 
		return "naim/index_old";
	}

}
